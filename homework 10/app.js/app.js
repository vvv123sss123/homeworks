"use strict"

let numberOne = +prompt("Введите первое число");
let numberTwo = +prompt("Введите второе число");
let mathSymbol = prompt ("Введите действие");

function res(numberOne, numberTwo, mathSymbol) {
    switch (mathSymbol) {
        case "+" : 
        return numberOne + numberTwo ;
        case "-" : 
        return numberOne - numberTwo;
        case "*" : 
        return numberOne * numberTwo;
        case "/" : 
        return numberOne / numberTwo;
        default:
            alert ("Нет таких значений");
    }
}

console.log(res(numberOne, numberTwo, mathSymbol));