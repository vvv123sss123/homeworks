"use strict"


function createNewUser() {
    let name = prompt("Введите имя");
    let surname = prompt("Введите фамилию");
    let birthday = prompt("Ваша дата рождения", "день.месяц.год").split('.');
    let birthDate = new Date(birthday[2], birthday[1], birthday[0]);
    let now = new Date();
    const newUser = {
        name,
        surname,
        birthday,
        birthDate,
        getLogin(){
            return (this.name[0]+this.surname).toLowerCase();
        },
        getAge(){
            let age = Math.floor((Date.now() - birthDate.getTime()) / 1000 / (60 * 60 * 24) / 365.2425);
            return age
        },
        getPassword(){
            return this.name[0].toUpperCase() + this.surname.toLowerCase() + this.birthDate.getFullYear();
        },
    }
    return newUser
}
const user = createNewUser();
const logIn = user.getLogin();
const password = user.getPassword();
const  age = user.getAge();
console.log(logIn);
console.log(password);
console.log(age);
console.log(user);
